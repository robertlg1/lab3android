package cs.mad.flashcards.adapters

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.entities.Flashcard

class FlashcardSetDetailActivityAdapter(input: List<Flashcard>) : RecyclerView.Adapter<FlashcardSetDetailActivityAdapter.ViewHolder>() {

    val myData = input.toMutableList()

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // store view references as properties using findViewById on view
        val textview = view.findViewById<TextView>(R.id.my_text2)
        val textView2 = view.findViewById<TextView>(R.id.my_text3)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard_setdetat, viewGroup, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textview.text = myData[position].term
        holder.textView2.text = myData[position].definition

    }

    override fun getItemCount(): Int {
        return myData.size
    }
}