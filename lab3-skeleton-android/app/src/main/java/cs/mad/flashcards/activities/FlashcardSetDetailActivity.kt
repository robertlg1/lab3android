package cs.mad.flashcards.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.adapters.FlashcardSetDetailActivityAdapter
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet

class FlashcardSetDetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flashcard_set_detail)

        val recycler = findViewById<RecyclerView>(R.id.Recyclere)
        val abutton = findViewById<Button>(R.id.addbutton2)
        val dbutton = findViewById<Button>(R.id.deletebutton2)
        var fc = Flashcard.getHardcodedFlashcards()

        abutton.setOnClickListener(){
            fc = ab()
            recycler.adapter = FlashcardSetDetailActivityAdapter(fc)
            recycler.layoutManager = LinearLayoutManager(this)
            recycler.adapter?.notifyDataSetChanged()
        }

        recycler.adapter = FlashcardSetDetailActivityAdapter(fc)
        recycler.layoutManager = LinearLayoutManager(this)


    }

    fun ab(): List<Flashcard>{
        val lst = mutableListOf<Flashcard>()
        lst.addAll(Flashcard.getHardcodedFlashcards())
        lst.add(Flashcard("term 11", "defintion 11"))
        return lst

    }

}